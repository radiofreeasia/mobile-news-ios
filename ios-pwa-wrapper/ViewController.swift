//
//  ViewController.swift
//  ios-pwa-wrapper
//
//  Created by Martin Kainzbauer on 25/10/2017.
//  Copyright © 2017 Martin Kainzbauer. All rights reserved.
//

import UIKit
import WebKit
import QuickLook
import PDFKit
import MobileCoreServices
//import Alamofire

enum SchemeError: Error {
    case noUrl
    case generalError
}

class ViewController: UIViewController, WKScriptMessageHandler {

    // MARK: Outlets
    @IBOutlet weak var leftButton: UIBarButtonItem!
    @IBOutlet weak var rightButton: UIBarButtonItem!
    @IBOutlet weak var webViewContainer: UIView!
    @IBOutlet weak var offlineView: UIView!
    @IBOutlet weak var offlineIcon: UIImageView!
    @IBOutlet weak var offlineButton: UIButton!
    @IBOutlet weak var activityIndicatorView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: Globals
    var webView: WKWebView!
    var tempView: WKWebView!
    var progressBar : UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            view.backgroundColor = UIColor.systemBackground
        } else {
            view.backgroundColor = UIColor.white
        }
        // Do any additional setup after loading the view, typically from a nib.
        self.title = appTitle
        setupApp()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // UI Actions
    // handle back press
    @IBAction func onLeftButtonClick(_ sender: Any) {
        if (webView.canGoBack) {
            webView.goBack()
            // fix a glitch, as the above seems to trigger observeValue -> WKWebView.isLoading
            activityIndicatorView.isHidden = true
            activityIndicator.stopAnimating()
        } else {
            // exit app
            UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
        }
    }
    // open menu in page, or fire alternate function on large screens
    @IBAction func onRightButtonClick(_ sender: Any) {
        if (changeMenuButtonOnWideScreens && isWideScreen()) {
            webView.evaluateJavaScript(alternateRightButtonJavascript, completionHandler: nil)
        } else {
            webView.evaluateJavaScript(menuButtonJavascript, completionHandler: nil)
        }
    }
    // reload page from offline screen
    @IBAction func onOfflineButtonClick(_ sender: Any) {
        offlineView.isHidden = true
        webViewContainer.isHidden = false
        loadAppUrl()
    }
    
    // Observers for updating UI
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (keyPath == #keyPath(WKWebView.isLoading)) {
            // show activity indicator

            /*
            // this causes troubles when swiping back and forward.
            // having this disabled means that the activity view is only shown on the startup of the app.
            // ...which is fair enough.
            if (webView.isLoading) {
                activityIndicatorView.isHidden = false
                activityIndicator.startAnimating()
            }
            */
        }
        if (keyPath == #keyPath(WKWebView.estimatedProgress)) {
            progressBar.progress = Float(webView.estimatedProgress)
            rightButton.isEnabled = (webView.estimatedProgress == 1)
        }
    }
    
    // Initialize WKWebView
    func setupWebView() {
        let webViewConfiguration = WKWebViewConfiguration()
        // init this view controller to receive JavaScript callbacks
        webViewConfiguration.userContentController.add(self, name: "openDocument")
        webViewConfiguration.userContentController.add(self, name: "jsError")
        webViewConfiguration.userContentController.add(self, name: "logger")
        webViewConfiguration.userContentController.add(self, name: "downloader")
        webViewConfiguration.userContentController.add(self, name: "setShowFullscreenVideo")
        webViewConfiguration.allowsInlineMediaPlayback = true
        
        //webViewConfiguration.setURLSchemeHandler(self, forURLScheme: "mirroreds")
        
        let source = """
            var orig = console.log
            console.log = function log() {
                orig.apply(console, arguments)
                window.webkit.messageHandlers.logger.postMessage(Array.from(arguments).join(' '));
            }
            window.NativeInterface = {
                canDownloadUrl: function() {
                    console.log("can download url called!");
                    return false;
                },
                downloadUrl: function(title,type,url) {
                    window.webkit.messageHandlers.downloader.postMessage({title: title, type: type, url: url});
                },
                canDownload: function() {
                    console.log("can download called!");
                    return false;
                },
                download: function(title,type,mediaBytes) {
                    console.log("Download called, this is a mistake!");
                },
                setShowFullscreenVideo: function(show) {
                    window.webkit.messageHandlers.setShowFullscreenVideo.postMessage(show);
                }
            }
        """
        let userScript = WKUserScript(source: source,
                                      injectionTime: .atDocumentStart,
                                      forMainFrameOnly: true)
        webViewConfiguration.userContentController.addUserScript(userScript)
        
        // set up webview
        webView = WKWebView(frame: CGRect(x: 0, y: 0, width: webViewContainer.frame.width, height: webViewContainer.frame.height), configuration: webViewConfiguration)
        webView.navigationDelegate = self
        webView.uiDelegate = self
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webViewContainer.addSubview(webView)
        // settings
        webView.allowsBackForwardNavigationGestures = true
        webView.configuration.preferences.javaScriptEnabled = true
        if #available(iOS 10.0, *) {
            webView.configuration.ignoresViewportScaleLimits = false
        }
        // user agent
        if #available(iOS 9.0, *) {
            if (useCustomUserAgent) {
                webView.customUserAgent = customUserAgent
            }
            if (useUserAgentPostfix) {
                if (useCustomUserAgent) {
                    webView.customUserAgent = customUserAgent + " " + userAgentPostfix
                } else {
                    tempView = WKWebView(frame: .zero)
                    tempView.evaluateJavaScript("navigator.userAgent", completionHandler: { (result, error) in
                        if let resultObject = result {
                            self.webView.customUserAgent = (String(describing: resultObject) + " " + userAgentPostfix)
                            self.tempView = nil
                        }
                    })
                }
            }
            webView.configuration.applicationNameForUserAgent = ""
        }
        
        // bounces
        webView.scrollView.backgroundColor = navigationBarColor
        webView.scrollView.bounces = enableBounceWhenScrolling
        webView.scrollView.contentInsetAdjustmentBehavior = .never

        // init observers
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.isLoading), options: NSKeyValueObservingOptions.new, context: nil)
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: NSKeyValueObservingOptions.new, context: nil)
        
        // Add pull to refresh
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(reloadWebView(_:)), for: .valueChanged)
        webView.scrollView.addSubview(refreshControl)
    }
    
    @objc func reloadWebView(_ sender: UIRefreshControl) {
        webView.reload()
        sender.endRefreshing()
    }
    
    // Initialize UI elements
    // call after WebView has been initialized
    func setupUI() {
        // leftButton.isEnabled = false

        // progress bar
        progressBar = UIProgressView(frame: CGRect(x: 0, y: 0, width: webViewContainer.frame.width, height: 40))
        progressBar.autoresizingMask = [.flexibleWidth]
        progressBar.progress = 0.0
        progressBar.tintColor = progressBarColor
        webView.addSubview(progressBar)
        
        // activity indicator
        activityIndicator.color = activityIndicatorColor
        activityIndicator.startAnimating()
        
        // offline container
        offlineIcon.tintColor = offlineIconColor
        offlineButton.tintColor = buttonColor
        offlineView.isHidden = true
        
        // setup navigation bar
        if (forceLargeTitle) {
            if #available(iOS 11.0, *) {
                navigationItem.largeTitleDisplayMode = UINavigationItem.LargeTitleDisplayMode.always
            }
        }
        if (useLightStatusBarStyle) {
            self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        }
        
        // handle menu button changes
        /// set default
        rightButton.title = menuButtonTitle
        /// update if necessary
        updateRightButtonTitle(invert: false)
        /// create callback for device rotation
        let deviceRotationCallback : (Notification) -> Void = { _ in
            // this fires BEFORE the UI is updated, so we check for the opposite orientation,
            // if it's not the initial setup
            self.updateRightButtonTitle(invert: true)
        }
        /// listen for device rotation
        NotificationCenter.default.addObserver(forName: UIDevice.orientationDidChangeNotification, object: nil, queue: .main, using: deviceRotationCallback)

        /*
        // @DEBUG: test offline view
        offlineView.isHidden = false
        webViewContainer.isHidden = true
        */
    }

    // load startpage
    func loadAppUrl() {
        var urlString = Config.appUrl
        if urlString.isEmpty {
            urlString = UserDefaults.standard.string(forKey: "appUrl") ?? ""
        }
        if urlString.isEmpty {
            let alert = UIAlertController(title: "Enter Test URL", message: "", preferredStyle: .alert)
            alert.addTextField { (textField) in
                textField.placeholder = "Enter URL here"
            }

            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                guard let textField = alert?.textFields?[0], let userText = textField.text else { return }
                UserDefaults.standard.set(userText, forKey: "appUrl")
                let urlRequest = URLRequest(url: URL(string: userText)!)
                self.webView.load(urlRequest)
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let urlRequest = URLRequest(url: URL(string: urlString)!)
        webView.load(urlRequest)
    }
    
    // Initialize App and start loading
    func setupApp() {
        setupWebView()
        setupUI()
        loadAppUrl()
    }
    
    // Cleanup
    deinit {
        webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.isLoading))
        webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress))
        NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    // Helper method to determine wide screen width
    func isWideScreen() -> Bool {
        // this considers device orientation too.
        if (UIScreen.main.bounds.width >= wideScreenMinWidth) {
            return true
        } else {
            return false
        }
    }
    
    // UI Helper method to update right button text according to available screen width
    func updateRightButtonTitle(invert: Bool) {
        if (changeMenuButtonOnWideScreens) {
            // first, check if device is wide enough to
            if (UIScreen.main.fixedCoordinateSpace.bounds.height < wideScreenMinWidth) {
                // long side of the screen is not long enough, don't need to update
                return
            }
            // second, check if both portrait and landscape would fit
            if (UIScreen.main.fixedCoordinateSpace.bounds.height >= wideScreenMinWidth
                && UIScreen.main.fixedCoordinateSpace.bounds.width >= wideScreenMinWidth) {
                // both orientations are considered "wide"
                rightButton.title = alternateRightButtonTitle
                return
            }
            
            // if we land here, check the current screen width.
            // we need to flip it around in some cases though, as our callback is triggered before the UI is updated
            let changeToAlternateTitle = invert
                ? !isWideScreen()
                : isWideScreen()
            if (changeToAlternateTitle) {
                rightButton.title = alternateRightButtonTitle
            } else {
                rightButton.title = menuButtonTitle
            }
        }
    }
}

// WebView Event Listeners
extension ViewController: WKNavigationDelegate {
    // didFinish
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        // set title
        if (changeAppTitleToPageTitle) {
            navigationItem.title = webView.title
        }
        webViewContainer.isHidden = false
        
        // hide progress bar after initial load
        progressBar.isHidden = true
        // hide activity indicator
        activityIndicatorView.isHidden = true
        activityIndicator.stopAnimating()
        
        if #available(iOS 14.0, *) {
            webView.evaluateJavaScript("window.native.fromNative('native');", in: nil, in: .page)
        } else {
            // Fallback on earlier versions
            webView.evaluateJavaScript("window.native.fromNative('native');")
        }
    }
    // didFailProvisionalNavigation
    // == we are offline / page not available
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        // show offline screen
        offlineView.isHidden = false
        webViewContainer.isHidden = true
    }
}

// WebView additional handlers
extension ViewController: WKUIDelegate {

    // handle links opening in new tabs
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if (navigationAction.targetFrame == nil) {
            webView.load(navigationAction.request)
        }
        return nil
    }
    
    // restrict navigation to target host, open external links in 3rd party apps
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let requestUrl = navigationAction.request.url {
            if requestUrl.absoluteString.starts(with: "blob:") {
                executeDocumentDownloadScript(forAbsoluteUrl: requestUrl.absoluteString)
                decisionHandler(.cancel)
            } else
            if let _ = requestUrl.host {
//                if allowedOrigins.contains(where: { (allowedOrigin) -> Bool in
//                    return requestHost.range(of: allowedOrigin) != nil
//                }) {
                    decisionHandler(.allow)
//                } else {
//                    decisionHandler(.cancel)
//                }
            } else {
                decisionHandler(.cancel)
            }
        }
    }
    
//    @available(iOS 13.0, *)
//    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, preferences: WKWebpagePreferences, decisionHandler: @escaping (WKNavigationActionPolicy, WKWebpagePreferences) -> Void) {
//         if let requestUrl = navigationAction.request.url {
//            print("Url \(requestUrl)")
//        }
//        decisionHandler(.allow, preferences)
//    }
    
    private func executeDocumentDownloadScript(forAbsoluteUrl absoluteUrl : String) {
        webView.evaluateJavaScript("""
            (async function download() {
                const url = '\(absoluteUrl)';
                try {
                    let blob = await fetch(url).then(r => r.blob());
                    var reader = new FileReader();
                    reader.onload = function() {
                        var dataUrl = reader.result;
                        var base64 = dataUrl.split(",")[1];
                        window.webkit.messageHandlers.openDocument.postMessage({data: `${base64}`, type: blob.type});
                    };
                    reader.readAsDataURL(blob);
                } catch (err) {
                    window.webkit.messageHandlers.jsError.postMessage(`JSError while downloading document, url: ${url}, err: ${err}`)
                }
            })();
            // null is needed here as this eval returns the last statement and we can't return a promise
            null;
        """) { (result, err) in
            if (err != nil) {
                debugPrint("JS ERR: \(String(describing: err))")
            }
        }
    }

    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "openDocument", let dict = message.body as? [String:String], let data = dict["data"], let mime = dict["type"] {
            previewDocument(messageBody: data, mime: mime)
        } else if message.name == "jsError" {
            debugPrint(message.body as! String)
        } else if message.name == "logger" {
            debugPrint(message.body as! String)
        } else if message.name == "setShowFullscreenVideo" {
            let fullscreen = message.body as! Bool
            if fullscreen {
                view.backgroundColor = UIColor.black
            } else {
                if #available(iOS 13.0, *) {
                    view.backgroundColor = UIColor.systemBackground
                } else {
                    view.backgroundColor = UIColor.white
                }
            }
        } else if message.name == "downloader", let dict = message.body as? [String:String], let title = dict["title"], let urlString = dict["url"], let url = URL(string: urlString) {
           let sharedObjects:[AnyObject] = [title as AnyObject,url as AnyObject]
           let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
           activityViewController.popoverPresentationController?.sourceView = self.view
           self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    private func previewDocument(messageBody: String, mime: String) {
        guard let data = Data(base64Encoded: messageBody),
              let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, mime as CFString, nil)?.takeRetainedValue(),
                let ext = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassFilenameExtension)
        else {
            debugPrint("Could not construct data from base64")
            return
        }

        var filename = "download." + (ext.takeRetainedValue() as String)
        if #available(iOS 11.0, *) {
            if mime == "application/pdf", let pdfDoc = PDFDocument(data: data) {
                filename = (pdfDoc.documentAttributes?[PDFDocumentAttribute.titleAttribute] as? String)?.appending(".pdf") ?? filename
            }
        }
        
        let localFileURL = FileManager.default.temporaryDirectory.appendingPathComponent(filename.removingPercentEncoding ?? filename)
                do {
                    try data.write(to: localFileURL);
                } catch {
                    debugPrint(error)
                    return
                }
        
        let di = UIDocumentInteractionController(url: localFileURL)
        di.uti = uti as String
        di.delegate = self
        di.presentPreview(animated: true)
    }
}

extension ViewController: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
}

//extension ViewController: WKURLSchemeHandler {
//    func webView(_ webView: WKWebView, start urlSchemeTask: WKURLSchemeTask) {
//        guard let url = urlSchemeTask.request.url, var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
//            urlSchemeTask.didFailWithError(SchemeError.noUrl)
//            return
//        }
//
//        webView.evaluateJavaScript("window.getCurrentProxy(false)", completionHandler: { (result, err) in
//            guard
//                err == nil,
//                let proxy = result as? String,
//                let proxyComponents = URLComponents(string: proxy)
//                else {
//                urlSchemeTask.didFailWithError(SchemeError.generalError)
//                return
//            }
//
//            urlComponents.scheme = proxyComponents.scheme
//            urlComponents.host = proxyComponents.host
//            guard let newUrl = try? urlComponents.asURL() else {
//                urlSchemeTask.didFailWithError(SchemeError.generalError)
//                return
//            }
//
//            DispatchQueue.global().async {
//                    AF.request(newUrl).response { response in
//                        urlSchemeTask.didReceive(response.response!)
//                        urlSchemeTask.didReceive(response.data!)
//                        urlSchemeTask.didFinish()
//                    }
//            }
//        })
//    }
//
//    func webView(_ webView: WKWebView, stop urlSchemeTask: WKURLSchemeTask) {
//    }
//}
